<?php

namespace App\Http\Controllers;

use App\Models\JobApplication;
use Illuminate\Http\Request;
use App\Http\Requests\JobFormRequest;
use App\Models\Language;
use App\Models\Technology;
use App\Models\State;
use App\Models\RelationStatus;
use App\Models\Department;

class JobApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::active()->with(['language_attribue'=>function($q){
            $q->active();
        }])->get();
        $technologies = Technology::active()->with(['technology_attribute'=>function($q){
            $q->active();
        }])->get();
        $states = State::all();
        $relation_status = RelationStatus::all();
        $departments = Department::all();
        return view('web.index',compact('languages','technologies','states','relation_status','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobFormRequest $request)
    {

        // dd($request->all());
        if(auth()->check() && auth()->user()->isAdmin == "1"){
            \Toastr::error('Admin cannot submit this form', 'Error');
            return redirect()->back();
        }
        /* Personal Detail */
        $data["first_name"] = $request->first_name;
        $data["last_name"] = $request->last_name;
        $data["email"] = $request->email;
        $data["designation"] = $request->designation;
        $data["phone"] = $request->phone;
        $data["zip_code"] = $request->zip_code;
        $data["address_1"] = $request->address_1;
        $data["address_2"] = $request->address_2 ?? null;
        $data["state"] = $request->state;
        $data["city"] = $request->city;
        $data["gender"] = $request->gender;
        $data["relation_status"] = $request->relation_status;
        $data["dob"] = date('Y-m-d',strtotime($request->dob));

        /* Education */
        $data["ssc_board_name"] = $request->ssc_board_name;
        $data["ssc_passing_year"] = $request->ssc_passing_year;
        $data["ssc_percentage"] = $request->ssc_percentage;
        $data["hsc_board_name"] = $request->hsc_board_name;
        $data["hsc_passing_year"] = $request->hsc_passing_year;
        $data["hsc_percentage"] = $request->hsc_percentage;
        $data["university_course_name"] = $request->university_course_name;
        $data["university_name"] = $request->university_name;
        $data["university_passing_year"] = $request->university_passing_year;
        $data["university_percentage"] = $request->university_percentage;
        $data["master_university_course_name"] = $request->master_university_course_name;
        $data["master_university_name"] = $request->master_university_name;
        $data["master_university_passing_year"] = $request->master_university_passing_year;
        $data["master_university_percentage"] = $request->master_university_percentage;

        /* Work Experince */
        $data["company_name"] = json_encode($request->company_name);
        $data["company_designation"] = json_encode($request->company_designation);
        $from_experience = $request->from_experience;
        foreach($from_experience as $fe){
            $fr[] = date('Y-m-d',strtotime($fe));
        }
        $data["from_experience"] = json_encode($fr);

        $to_experience = $request->to_experience;
        foreach($to_experience as $te){
            $tr[] = date('Y-m-d',strtotime($te));
        }
        $data["to_experience"] = json_encode($tr);

        // language
        $data["language"] = json_encode($request->language);        
        $data["language_attribute_1"] = $request->language_attribute_1 != null ? json_encode($request->language_attribute_1) : null;
        $data["language_attribute_2"] = $request->language_attribute_2 != null ? json_encode($request->language_attribute_2) : null;
        $data["language_attribute_3"] = $request->language_attribute_3 != null ? json_encode($request->language_attribute_3) : null;

        // technology
        $data["technology"] = json_encode($request->technology);
        $data["technology_attribute_1"] = $request->technology_attribute_1 != null ? $request->technology_attribute_1 : null;
        $data["technology_attribute_2"] = $request->technology_attribute_2 != null ? $request->technology_attribute_2 : null;
        $data["technology_attribute_3"] = $request->technology_attribute_3 != null ? $request->technology_attribute_3 : null;

        // reference contact
        $data["contact_name"] = $request->contact_name != null ? $request->contact_name : null;
        $data["contact_number"] = $request->contact_number != null ? $request->contact_number : null;
        $data["contact_relation"] = $request->contact_relation != null ? $request->contact_relation : null;

        // preference
        $data['prefered_location'] = $request->prefered_location;
        $data['notice_period'] = $request->notice_period;
        $data['current_ctc'] = $request->current_ctc;
        $data['expected_ctc'] = $request->expected_ctc;
        $data['department'] = $request->department;
        // dd($data);
        $result = JobApplication::create($data);
        if($result){
            \Toastr::success('Form submitted successfully', 'Success');
        }else{
            \Toastr::error('Form not submitted successfully', 'Error');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobApplication  $jobApplication
     * @return \Illuminate\Http\Response
     */
    public function show(JobApplication $jobApplication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobApplication  $jobApplication
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApplication $jobApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobApplication  $jobApplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobApplication $jobApplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobApplication  $jobApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobApplication $jobApplication)
    {
        //
    }
}
