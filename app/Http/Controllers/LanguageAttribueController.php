<?php

namespace App\Http\Controllers;

use App\Models\LanguageAttribue;
use Illuminate\Http\Request;

class LanguageAttribueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LanguageAttribue  $languageAttribue
     * @return \Illuminate\Http\Response
     */
    public function show(LanguageAttribue $languageAttribue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LanguageAttribue  $languageAttribue
     * @return \Illuminate\Http\Response
     */
    public function edit(LanguageAttribue $languageAttribue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LanguageAttribue  $languageAttribue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LanguageAttribue $languageAttribue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LanguageAttribue  $languageAttribue
     * @return \Illuminate\Http\Response
     */
    public function destroy(LanguageAttribue $languageAttribue)
    {
        //
    }
}
