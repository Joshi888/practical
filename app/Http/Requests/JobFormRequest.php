<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'designation' => 'required',
            'phone' => 'required',
            'zip_code' => 'required',
            'address_1' => 'required',
            'address_2' => 'max:255',
            'state' => 'required',
            'city' => 'required',
            'gender' => 'required',
            'relation_status' => 'required',
            'dob' => 'required',
            'ssc_board_name' => 'required',
            'ssc_passing_year' => 'required',
            'ssc_percentage' => 'required',
            'hsc_board_name' => 'required',
            'hsc_passing_year' => 'required',
            'hsc_percentage' => 'required',
            'university_course_name' => 'required',
            'university_name' => 'required',
            'university_passing_year' => 'required',
            'university_percentage' => 'required',
            'master_university_course_name' => 'required',
            'master_university_name' => 'required',
            'master_university_passing_year' => 'required',
            'master_university_percentage' => 'required',
            'company_name.*' => 'required',
            'company_designation.*' => 'required',
            'from_experience.*' => 'required',
            'to_experience.*' => 'required',
            'prefered_location' => 'required',
            'notice_period' => 'required',
            'current_ctc' => 'required',
            'expected_ctc' => 'required',
            'department' => 'required',

        ];
    }
}
