<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'designation',
        'phone',
        'zip_code',
        'address_1',
        'address_2',
        'state',
        'city',
        'gender',
        'relation_status',
        'dob',
        'ssc_board_name',
        'ssc_passing_year',
        'ssc_percentage',
        'hsc_board_name',
        'hsc_passing_year',
        'hsc_percentage',
        'university_course_name',
        'university_name',
        'university_passing_year',
        'university_percentage',
        'master_university_course_name',
        'master_university_name',
        'master_university_passing_year',
        'master_university_percentage',
        'company_name',
        'company_designation',
        'from_experience',
        'to_experience',
        'language',
        'language_attribute_1',
        'language_attribute_2',
        'language_attribute_3',
        'technology',
        'technology_attribute_1',
        'technology_attribute_2',
        'technology_attribute_3',
        'contact_name',
        'contact_number',
        'contact_relation',
        'prefered_location',
        'notice_period',
        'current_ctc',
        'expected_ctc',
        'department',
    ];

}
