<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\LanguageAttribue;

class Language extends Model
{
    use HasFactory;

    public function scopeActive($query){
    	return $query->where('isActive','1');
    }

    public function language_attribue(){
    	return $this->hasMany(LanguageAttribue::class);
    }
}
