<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TechnologyAttribute;

class Technology extends Model
{
    use HasFactory;

    public function scopeActive($query){
    	return $query->where('isActive','1');
    }

    public function technology_attribute(){
    	return $this->hasMany(TechnologyAttribute::class);
    }
}
