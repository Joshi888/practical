<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applications', function (Blueprint $table) {
            $table->id();
            $table->string("first_name");
            $table->string("last_name");
            $table->string("email");
            $table->string("designation");
            $table->string("phone",20);
            $table->string("zip_code",10);
            $table->string("address_1");
            $table->string("address_2")->default(null);
            // $table->bigInteger('state_id')->unsigned()->index();
            // $table->foreign('state_id')->references('id')->on('state')->onDelete('cascade');
            $table->integer("state");
            $table->string("city");
            $table->string("gender");
            // $table->integer('relation_status_id')->unsigned()->index();
            // $table->foreign('relation_status_id')->references('id')->on('relation_status')->onDelete('cascade');
            $table->integer("relation_status");
            $table->date("dob");
            $table->string("ssc_board_name");
            $table->string("ssc_passing_year");
            $table->string("ssc_percentage");
            $table->string("hsc_board_name");
            $table->string("hsc_passing_year");
            $table->string("hsc_percentage");
            $table->string("university_course_name");
            $table->string("university_name");
            $table->string("university_passing_year");
            $table->string("university_percentage");
            $table->string("master_university_course_name");
            $table->string("master_university_name");
            $table->string("master_university_passing_year");
            $table->string("master_university_percentage");
            $table->string("company_name");
            $table->string("company_designation");
            $table->string("from_experience");
            $table->string("to_experience");
            $table->string("language");
            $table->string("language_attribute_1");
            $table->string("language_attribute_2");
            $table->string("language_attribute_3");
            $table->string("technology");
            $table->string("technology_attribue_1");
            $table->string("technology_attribue_2");
            $table->string("technology_attribue_3");
            $table->string("contact_name");
            $table->string("contact_number");
            $table->string("contact_relation");
            $table->string("prefered_location");
            $table->string("notice_period");
            $table->string("current_ctc");
            $table->string("expected_ctc");
            // $table->integer('department_id')->unsigned()->index();
            // $table->foreign('department_id')->references('id')->on('department')->onDelete('cascade');
            $table->integer("department");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applications');
    }
}
