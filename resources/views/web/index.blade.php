@extends('layouts.form')

@section('extra_css')
<style type="text/css">
    label.error{
        color: red !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
    	<form name="job_application_form" id="job_application_form" method="post" action="{{ route('store') }}">
    		@csrf
	        <div class="col-md-12">
	        	<!-- Basic detail section -->
	            <div class="card">
	                <div class="card-header"><h5>{{ __('Basic Detail') }}</h5></div>

	                <div class="card-body">
	                	<div class="row">
	                		<div class="col-md-6">
	                			<div class="form-group">
			                    	<label>First Name</label>
			                    	<input type="text" class="form-control" name="first_name" placeholder="Enter first name">
			                    </div>
			                </div>
			                <div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Last Name</label>
			                    	<input type="text" class="form-control" name="last_name" placeholder="Enter last name">
			                    </div>
	                		</div>
	                	</div>
	                	<div class="row">
	                		<div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Email</label>
			                    	<input type="email" class="form-control" name="email" placeholder="Enter email">
			                    </div>
			                </div>
	                		<div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Designation</label>
			                    	<input type="text" class="form-control" name="designation" placeholder="Enter designation">
			                    </div>
			                </div>
			            </div>
			            <div class="row">
	                		<div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Phone Number</label>
			                    	<input type="number" class="form-control" name="phone" placeholder="Enter number">
			                    </div>
			                </div>
			                <div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Zip Code</label>
			                    	<input type="text" class="form-control" name="zip_code" placeholder="Enter zip code">
			                    </div>
			                </div>
			            </div>
			            <div class="row">
			                <div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Address 1</label>
			                    	<textarea class="form-control" name="address_1" placeholder="Enter your address" style="resize: none;"></textarea>
			                    </div>
			                </div>
			                <div class="col-md-6">
			                    <div class="form-group">
			                    	<label>Address 2</label>
			                    	<textarea class="form-control" name="address_2" placeholder="Enter your address" style="resize: none;"></textarea>
			                    </div>
			                </div>
			            </div>
			            <div class="row">
			            	<div class="col-md-6">
			                    <div class="form-group">
			                    	<label>State</label>
			                    	<select name="state" class="form-control">
			                    		<option value="">-Select State-</option>
			                    		@foreach($states as $st)
			                    			<option value="{{$st->id}}">{{$st->state}}</option>
			                    		@endforeach
			                    	</select>
			                    </div>
			                </div>
			                <div class="col-md-6">
			                    <div class="form-group">
			                    	<label>City</label>
			                    	<input type="text" class="form-control" name="city" placeholder="Enter city">
			                    </div>
			                </div>
			            </div>
			            <div class="row">
			            	<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Gender</label>
			                    	<br>
			                    	<label class="radio-inline" for="male">
								      <input type="radio" name="gender" id="male" value="male">&nbsp;&nbsp;Male
								    </label>
								    &nbsp;&nbsp;
								    <label class="radio-inline" for="female">
								      <input type="radio" name="gender" id="female" value="female">&nbsp;&nbsp;Female
								    </label>
			                    	<!-- <input type="radio" name="gender" value="male">Male -->
			                    </div>
			                </div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Relationship Status</label>
			                    	<select name="relation_status" class="form-control">
			                    		@foreach($relation_status as $rs)
			                    			<option value="{{$rs->id}}">{{$rs->name}}</option>
			                    		@endforeach
			                    	</select>
			                    </div>
			                </div>
			                <div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Date Of Birth</label>
			                    	<input type="text" class="form-control datepicker" name="dob" placeholder="Select date of birth">
			                    </div>
			                </div>
			            </div>
	                </div>
	            </div>
	            <hr>

	            <!-- Education section -->
	            <div class="card">
	                <div class="card-header"><h5>{{ __('Education Detail') }}</h5></div>

	                <div class="card-body">
	                	<p>SSC Result</p>
	                	<div class="row">
	                		<div class="col-md-4">
	                			<div class="form-group">
			                    	<label>Name Of Board</label>
			                    	<input type="text" class="form-control" name="ssc_board_name" placeholder="Enter board name">
			                    </div>
			                </div>
			                <div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Passing Year</label>
			                    	<input type="text" class="form-control" name="ssc_passing_year" placeholder="Enter passing year">
			                    </div>
	                		</div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Percentage</label>
			                    	<input type="text" class="form-control" name="ssc_percentage" placeholder="Enter percentage">
			                    </div>
	                		</div>
	                	</div>
	                	<hr>

	                	<p>HSC / Diploma Result</p>
	                	<div class="row">
	                		<div class="col-md-4">
	                			<div class="form-group">
			                    	<label>Name Of Board</label>
			                    	<input type="text" class="form-control" name="hsc_board_name" placeholder="Enter board name">
			                    </div>
			                </div>
			                <div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Passing Year</label>
			                    	<input type="text" class="form-control" name="hsc_passing_year" placeholder="Enter passing year">
			                    </div>
	                		</div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Percentage</label>
			                    	<input type="text" class="form-control" name="hsc_percentage" placeholder="Enter percentage">
			                    </div>
	                		</div>
	                	</div>
	                	<hr>

	                	<p>Bachelor Degree</p>
	                	<div class="row">
	                		<div class="col-md-4">
	                			<div class="form-group">
			                    	<label>Course Name</label>
			                    	<input type="text" class="form-control" name="university_course_name" placeholder="Enter board name">
			                    </div>
			                </div>
			                <div class="col-md-4">
			                    <div class="form-group">
			                    	<label>University</label>
			                    	<input type="text" class="form-control" name="university_name" placeholder="Enter university">
			                    </div>
	                		</div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Passing Year</label>
			                    	<input type="text" class="form-control" name="university_passing_year" placeholder="Enter passing year">
			                    </div>
	                		</div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Percentage</label>
			                    	<input type="text" class="form-control" name="university_percentage" placeholder="Enter percentage">
			                    </div>
	                		</div>
	                	</div>
	                	<hr>

	                	<p>Master Degree</p>
	                	<div class="row">
	                		<div class="col-md-4">
	                			<div class="form-group">
			                    	<label>Course Name</label>
			                    	<input type="text" class="form-control" name="master_university_course_name" placeholder="Enter board name">
			                    </div>
			                </div>
			                <div class="col-md-4">
			                    <div class="form-group">
			                    	<label>University</label>
			                    	<input type="text" class="form-control" name="master_university_name" placeholder="Enter university">
			                    </div>
	                		</div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Passing Year</label>
			                    	<input type="text" class="form-control" name="master_university_passing_year" placeholder="Enter passing year">
			                    </div>
	                		</div>
	                		<div class="col-md-4">
			                    <div class="form-group">
			                    	<label>Percentage</label>
			                    	<input type="text" class="form-control" name="master_university_percentage" placeholder="Enter percentage">
			                    </div>
	                		</div>
	                	</div>
	                	<hr>
	                </div>

	                {{-- <div class="card-footer">
	                	<div class="float-right">
	                		<input type="button" name="submit" value="Next" class="next btn btn-primary">
	                	</div>
	                </div> --}}
	            </div>
	            <hr>

	            <!-- Work experice section -->
	            <div class="card">
	            	<div class="card-header"><h5>{{ __('Work Experience') }}</h5></div>
					
					<div class="card-body add_data">
			        	<div class="row" data-value="0">
			        		<div class="col-md-3">
			        			<div class="form-group">
			        				<label>Company Name</label>
			        				<input type="text" name="company_name[]" class="form-control" placeholder="Enter Company name" autocomplete="off">
			        			</div>
			        		</div>
			        		<div class="col-md-3">
			        			<div class="form-group">
				        			<label>Designation</label>
				        			<input type="text" name="company_designation[]" class="form-control" placeholder="Enter designation" autocomplete="off">
				        		</div>
			        		</div>
			        		<div class="col-md-2">
			        			<div class="form-group">
				        			<label>From</label>
				        			<input type="text" name="from_experience[]" class="form-control datepicker" placeholder="Select date" autocomplete="off">
				        		</div>
			        		</div>
			        		<div class="col-md-2">
			        			<div class="form-group">
				        			<label>To</label>
				        			<input type="text" name="to_experience[]" class="form-control datepicker" placeholder="Select date" autocomplete="off">
				        		</div>
			        		</div>
			        		<div class="col-auto">
			        			<label>&nbsp;</label>
			        			<div class="add_btn">
			        				<a href="javascript:void(0);" onclick="addWorkBtn(0)" class="btn btn-primary add_work_btn"><i class="fa fa-plus-circle"></i></a>
			        			</div>
			        		</div>
			        	</div>
			        </div>
	            </div>
	            <hr>

	            <!-- Language and technolgoy section -->
	            <div class="row">
		    		<div class="col-md-6">
		    			<div class="card">
			            	<div class="card-header"><h5>{{ __('Language Known') }}</h5></div>

			                <div class="card-body">
			                	@foreach($languages as $value)
			                	<div class="row">
			                		<div class="col-md-3">
			                			<input type="checkbox" class="lang" name="language[]" id="lang_{{$value->id}}" value="{{$value->id}}" data-id="{{$value->id}}">
			                			<label for="lang_{{$value->id}}">{{ $value->name }}</label>
			                		</div>
			                		@foreach($value->language_attribue as $val)
			                			<div class="col-md-3 lang_attr" data-lang="{{$val->language_id}}">
				                			<input type="checkbox" id="lang_attr_{{$value->id."_".$val->id}}" name="language_attribute_{{$value->id}}[]" value="{{$val->id}}" disabled>
				                			<label for="lang_attr_{{$value->id."_".$val->id}}">{{ $val->name }}</label>
				                		</div>
			                		@endforeach
			                	</div>
			                	<br>
			                	@endforeach
			                </div>
			            </div>		
		    		</div>

		    		<div class="col-md-6">
		    			<div class="card">
			            	<div class="card-header"><h5>{{ __('Technology you Known') }}</h5></div>

			                <div class="card-body">
			                	@foreach($technologies as $tech)
			                	<div class="row">
			                		<div class="col-md-3">
			                			<input type="checkbox" class="tech" id="tech_{{$tech->id}}" name="technology[]" value="{{$tech->id}}" data-tid="{{$tech->id}}">
			                			<label for="tech_{{$tech->id}}">{{$tech->name}}</label>
			                		</div>
			                		@foreach($tech->technology_attribute as $ta)
				                		<div class="col-md-3 tech_attr" data-tech="{{$ta->technology_id}}">
				                			<input type="radio" id="tech_attr_{{$tech->id."_".$ta->id}}" name="technology_attribute_{{$tech->id}}" value="{{$ta->id}}" disabled>
				                			<label for="tech_attr_{{$tech->id."_".$ta->id}}">&nbsp;{{ $ta->name }}</label>
				                		</div>
			                		@endforeach
			                		<br>
			                	</div>
			                	@endforeach
			                </div>
			            </div>		
		    		</div>
		    	</div>
		    	<hr>

		    	<!-- Reference Contact section -->
		    	<div class="card">
		    		<div class="card-header"><h5>{{ __('Reference Contact') }}</h5></div>
		    		<div class="card-body">
		    			<div class="row">
		    				<div class="col-md-4">
		    					<div class="form-group">
			    					<label>Name</label>
			    					<input type="text" name="contact_name" placeholder="Enter name" class="form-control">
			    				</div>
		    				</div>
		    				<div class="col-md-4">
		    					<div class="form-group">
			    					<label>Contact Number</label>
			    					<input type="number" name="contact_number" placeholder="Enter number" class="form-control">
			    				</div>
		    				</div>
		    				<div class="col-md-4">
		    					<div class="form-group">
			    					<label>Relation</label>
			    					<input type="text" name="contact_relation" placeholder="Enter relation" class="form-control">
			    				</div>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    	<hr>

		    	<!-- Preference section -->
		    	<div class="card">
		    		<div class="card-header"><h5>{{ __('Preference') }}</h5></div>
		    		<div class="card-body">
		    			<div class="row">
		    				<div class="col-md-4">
		    					<div class="form-group">
		    						<label>Prefered Location</label>
		    						<select name="prefered_location" class="form-control">
		    							<option value="">-Select Location-</option>
		    							<option value="India">India</option>
		    							<option value="Usa">USA</option>
		    							<option value="Canada">Canada</option>
		    						</select>
		    					</div>
		    				</div>
		    				<div class="col-md-4">
		    					<div class="row">
		    						<div class="col-md-12">
		    							<div class="form-group">
		    								<label>Notice Period</label>
		    								<input type="text" name="notice_period" class="form-control" placeholder="Enter notice period">
		    							</div>
		    						</div>
		    						<div class="col-md-12">
		    							<div class="form-group">
		    								<label>Current CTC</label>
		    								<input type="text" name="current_ctc" class="form-control" placeholder="Enter current ctc">
		    							</div>
		    						</div>
		    						<div class="col-md-12">
		    							<div class="form-group">
		    								<label>Expected CTC</label>
		    								<input type="text" name="expected_ctc" class="form-control" placeholder="Enter expected ctc">
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    				<div class="col-md-4">
		    					<div class="form-group">
		    						<label>Department</label>
		    						<select name="department" class="form-control">
		    							<option value="">-Select Department</option>
		    							@foreach($departments as $ds)
		    								<option value="{{$ds->id}}">{{$ds->name}}</option>
		    							@endforeach
		    						</select>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
	        </div>

	        <br>
    		<div class="col-md-12">
	        	<input type="submit" name="submit" value="Submit" class="btn btn-primary float-right">
	        </div>	
        </form>
    </div>
</div>
@endsection

@section('extra_js')
<script type="text/javascript" defer="" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		$("#job_application_form").validate({
			rules:{
				first_name:{
					required:true
				},
				last_name:{
					required:true
				},
				email:{
					required:true,
					email:true
				},
				designation:{
					required:true
				},
				phone:{
					required:true,
					number:true
				},
				zip_code:{
					required:true,
					number:true
				},
				address_1:{
					required:true,
				},
				state:{
					required: true
				},
				city:{
					required: true
				},
				gender:{
					required:true
				},
				relation_status:{
					required:true
				},
				dob:{
					required:true
				},
				ssc_board_name:{
					required:true
				},
				ssc_passing_year:{
					required:true
				},
				ssc_percentage:{
					required:true
				},
				hsc_board_name:{
					required:true
				},
				hsc_passing_year:{
					required:true
				},
				hsc_percentage:{
					required:true
				},
				university_course_name:{
					required:true
				},
				university_name:{
					required:true
				},
				university_passing_year:{
					required:true
				},
				university_percentage:{
					required:true
				},
				master_university_course_name:{
					required:true
				},
				master_university_name:{
					required:true
				},
				master_university_passing_year:{
					required:true
				},
				master_university_percentage:{
					required:true
				},
				"company_name[]":{
					required:true
				},
				"company_designation[]":{
					required:true
				},
				"from_experience[]":{
					required:true
				},
				"to_experience[]":{
					required:true
				},
				"language[]":{
					required:true
				},
				/*"language_attribute[]":{
					required:true
				},*/
				"language_attribute_1[]":{
					required:true
				},
				"language_attribute_2[]":{
					required:true
				},
				"language_attribute_3[]":{
					required:true
				},
				"technology[]":{
					required:true
				},
				"technology_attribute_1":{
					required:true
				},
				"technology_attribute_2":{
					required:true
				},
				"technology_attribute_3":{
					required:true
				},
				prefered_location:{
					required:true
				},
				notice_period:{
					required:true,
					number:true
				},
				current_ctc:{
					required:true,
					number:true
				},
				expected_ctc:{
					required:true,
					number:true
				},
				department:{
					required:true
				}
			},
			messages:{
				first_name:{
					required:"Please enter first name"
				},
				last_name:{
					required:"Please enter last name"
				},
				email:{
					required:"Please enter email",
					email:"Please enter valid email"
				},
				designation:{
					required:"Please enter designation",
				},
				phone:{
					required:"Please enter phone number",
					number: "Please enter the valid number"
				},
				zip_code:{
					required:"Please enter zip code",
					number: "Please enter the valid zip code"
				},
				address_1:{
					required:"Please enter address",
				},
				state:{
					required: "Please select the state"
				},
				city:{
					required: "Please enter the city"
				},
				gender:{
					required: "Please select the gender"
				},
				relation_status:{
					required: "Please select the relation"
				},
				dob:{
					required: "Please enter the date of birth"
				},
				ssc_board_name:{
					required: "Please enter the board name"
				},
				ssc_passing_year:{
					required: "Please enter the passing year"
				},
				ssc_percentage:{
					required: "Please enter the percentage"
				},
				hsc_board_name:{
					required: "Please enter the board name"
				},
				hsc_passing_year:{
					required: "Please enter the passing year"
				},
				hsc_percentage:{
					required: "Please enter the percentage"
				},
				university_course_name:{
					required: "Please enter the course name"
				},
				university_name:{
					required: "Please enter the board name"
				},
				university_passing_year:{
					required: "Please enter the passing year"
				},
				university_percentage:{
					required: "Please enter the percentage"
				},
				master_university_course_name:{
					required: "Please enter the course name"
				},
				master_university_name:{
					required: "Please enter the board name"
				},
				master_university_passing_year:{
					required: "Please enter the passing year"
				},
				master_university_percentage:{
					required: "Please enter the percentage"
				},
				"company_name[]":{
					required: "Please enter the company name"
				},
				"company_designation[]":{
					required: "Please enter the designation"
				},
				"from_experience[]":{
					required: "Please enter the start date"
				},
				"to_experience[]":{
					required: "Please enter the end date"
				},
				"language[]":{
					required: "Please select the language"
				},
				"language_attribute[]":{
					required: "Please select the skills"
				},
				"technology[]":{
					required: "Please select the technology"
				},
				"technology_attribute[]":{
					required: "Please select the skills"
				},
				prefered_location:{
					required: "Please enter the prefered location"
				},
				notice_period:{
					required: "Please enter the notice period",
					number: "Please enter the valid notice period"
				},
				current_ctc:{
					required: "Please enter the current ctc",
					number: "Please enter the valid current ctc"
				},
				expected_ctc:{
					required: "Please enter the expected ctc",
					number: "Please enter the valid expected ctc"
				},
				department:{
					required: "Please select the department"
				}

			},
			errorPlacement: function(error, element) {
	            if (element.attr("type") == "radio") {
	                error.insertBefore(element);
	            } else {
	                error.insertAfter(element);
	            }
	        },
			onfocusout: function(element) {
	            this.element(element);
	        },
			/*submitHandler:function(form){
				let $form = $(form).serialize()
				console.log($form)
			}*/
		})

		$('.datepicker').datepicker({
		    format: 'dd/mm/yyyy',
		    // startDate: '-3d'
		});

		$(".lang").click(function(){
			let id = $(this).data("id")
			if($(this).is(":checked")){
				$(".lang_attr").filter(function(){
					return $(this).data("lang") == id
				}).find("input").prop('disabled', false);
			}else{
				$(".lang_attr").filter(function(){
					return $(this).data("lang") == id
				}).find("input").prop('disabled', true);
			}
		})

		$(".tech").click(function(){
			let tid = $(this).data("tid")
			if($(this).is(":checked")){
				$(".tech_attr").filter(function(){
					return $(this).data("tech") == tid
				}).find("input").prop('disabled', false);
			}else{
				$(".tech_attr").filter(function(){
					return $(this).data("tech") == tid
				}).find("input").prop('disabled', true);
			}
		})
	})

	let i = 0

	function addWorkBtn(){
		i++;
		let html = ''
		html += '<div class="row" data-value="'+i+'"><div class="col-md-3"><div class="form-group"><label>Company Name</label><input type="text" name="company_name[]" class="form-control" placeholder="Enter Company name"></div></div><div class="col-md-3"><div class="form-group"><label>Designation</label><input type="text" name="company_designation[]" class="form-control" placeholder="Enter designation"></div></div><div class="col-md-2"><div class="form-group"><label>From</label><input type="text" name="from_experience[]" class="form-control datepicker" placeholder="Select date"></div></div><div class="col-md-2"><div class="form-group"><label>To</label><input type="text" name="to_experience[]" class="form-control datepicker" placeholder="Select date"></div></div><div class="col-auto"><label>&nbsp;</label><div class="remove_btn"><a href="javascript:void(0);" class="btn btn-danger remove_work_btn-'+i+'" onclick="removeWorkBtn('+i+')"><i class="fa fa-minus-circle"></i></a></div></div></div>';
		$(".add_data").append(html)
		$('.datepicker').datepicker({
		    format: 'dd/mm/yyyy',
		    // startDate: '-3d'
		});
		$("#job_application_form").validate();
	}

	function removeWorkBtn(i){
		$(".row").filter(function(){
			return $(this).data("value") == i;
		}).remove()
		i--;
	}
</script>
@endsection
